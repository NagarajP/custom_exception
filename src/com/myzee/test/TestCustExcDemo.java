package com.myzee.test;

import com.myzee.customexc.TooOldException;
import com.myzee.customexc.TooYoungException;

public class TestCustExcDemo {

	public static void main(String[] args) throws TooOldException{
		// TODO Auto-generated method stub
//		int age = Integer.parseInt(args[0]);
		int age = 90;
		ageCheck(age);	//you can use throws on main method or use try-catch for this method call
	}
	
	public static void ageCheck(int age) throws TooOldException {
		if (age > 60) {
			try {
				throw new TooYoungException("Your too young");
			} catch (TooYoungException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (age < 18) {
			throw new TooOldException("Your too old");
		} else {
			System.out.println("U will get match soon");
		}
	}

}
